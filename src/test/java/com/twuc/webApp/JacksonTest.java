package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class JacksonTest {

    @Test
    void should_return_200_when_with_construct() throws Exception {
        ContractWithConstruct contractWithConstruct = new ContractWithConstruct();
        contractWithConstruct.setName("xiaoming");
        String json = new ObjectMapper().writeValueAsString(contractWithConstruct);
        assertEquals("{\"name\":\"xiaoming\"}", json);
    }

    @Test
    void should_return_200_when_without_construct() throws Exception {
        ContractWithoutConstruct contractWithoutConstruct = new ContractWithoutConstruct("xiaoming");
        String json = new ObjectMapper().writeValueAsString(contractWithoutConstruct);
        assertEquals("{\"name\":\"xiaoming\"}", json);
    }

    @Test
    void should_return_200_when_without_setter() throws Exception {
        ContractWithoutSetter contractWithoutSetter = new ContractWithoutSetter("xiaoming");
        String json = new ObjectMapper().writeValueAsString(contractWithoutSetter);
        assertEquals("{\"name\":\"xiaoming\"}", json);
    }

    @Test
    void should_return_400_when_with_only_setter() {
        assertThrows(InvalidDefinitionException.class, () -> {
            ContractWithSetter contractWithSetter = new ContractWithSetter("xiaoming");
            new ObjectMapper().writeValueAsString(contractWithSetter);
        });
    }
}
