package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void should_return_200_when_bind_param_to_Integer_type() throws Exception {
        mockMvc.perform(get("/api/users/2"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_200_when_bind_to_int_type() throws Exception {
        mockMvc.perform(get("/api/users/2/int"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_200_when_bind_two_params() throws Exception {
        mockMvc.perform(get("/api/users/3/books/1"))
                .andExpect(status().isOk());
    }

    @Test
    void should_bind_query_string_successfully() throws Exception {
        mockMvc.perform(get("/api/users?id=3"))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));
    }

    @Test
    void should_return_default_value_when_bind_query_string() throws Exception {
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }

    @Test
    void should_bind_collection_successfully() throws Exception {
        mockMvc.perform(get("/api/users/1/books?names=1,2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0]").value(1))
                .andExpect(jsonPath("$[1]").value(2));
    }

    @Test
    void should_return_date_when_bind_date_time() throws Exception {
        mockMvc.perform(post("/api/datetimes")
                .content("{\"dateTime\": \"2019-10-01T10:00:00Z\"}")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("\"2019-10-01T10:00:00Z\""));
    }

    @Test
    void should_return_date_when_bind_not_iso_date_time() throws Exception {
        mockMvc.perform(post("/api/datetimes")
                .content("{\"dateTime\": \"2019-10-01\"}")
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_name_not_null() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new User("xiaoming"));
        mockMvc.perform(post("/api/users")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_name_is_null() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new User());
        mockMvc.perform(post("/api/users")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_string_size_greater_than_2() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new User("xiaoming"));
        mockMvc.perform(post("/api/users")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_string_size_less_than_2() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new User("x"));
        mockMvc.perform(post("/api/users")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_year_of_birth_in_1000_and_9999() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new User(1000));
        mockMvc.perform(post("/api/users")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_year_of_birth_over_range() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new User(10000));
        mockMvc.perform(post("/api/users")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_email_is_valid() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new User("xiaoming", "zijie.jiang@thoughtworks.com"));
        mockMvc.perform(post("/api/users")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_email_is_not_valid() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new User("xiaoming", "zijie.jiang"));
        mockMvc.perform(post("/api/users")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }
}
