package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/users/{userId}")
    public void bindToInteger(@PathVariable Integer userId) {
    }

    @GetMapping("/users/{userId}/int")
    public void bindToInt(@PathVariable int userId) {
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public void bindTwoParam(@PathVariable("userId") Integer userId, @PathVariable("bookId") Integer bookId) {
    }

    @GetMapping("/users")
    public Integer bindQueryString(@RequestParam(defaultValue = "0") Integer id) {
        return id;
    }

    @GetMapping("/users/1/books")
    public List<String> bindCollection(@RequestParam List<String> names) {
        return names;
    }

    @PostMapping("/datetimes")
    public ZonedDateTime bindDateTime(@RequestBody DateTime time) {
        return time.getDateTime();
    }

    @PostMapping("/users")
    public User createUser(@RequestBody @Valid User user) {
        return user;
    }
}
