package com.twuc.webApp;

public class ContractWithoutSetter {
    private String name;

    public ContractWithoutSetter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
