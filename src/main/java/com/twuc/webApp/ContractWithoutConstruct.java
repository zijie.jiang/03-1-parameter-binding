package com.twuc.webApp;

public class ContractWithoutConstruct {
    private String name;

    public ContractWithoutConstruct(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
