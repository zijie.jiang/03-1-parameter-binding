package com.twuc.webApp;

public class ContractWithSetter {
    private String name;

    public ContractWithSetter(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
