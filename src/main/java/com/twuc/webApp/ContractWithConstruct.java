package com.twuc.webApp;

public class ContractWithConstruct {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
