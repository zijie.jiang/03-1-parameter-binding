package com.twuc.webApp;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {
    @NotNull
    @Size(min = 2)
    private String name;

    @Min(1000)
    @Max(9999)
    private Integer yearOfBirth;

    @Email
    private String email;

    public User() {
    }

    public User(String name) {
        this.email = "zijie.jiang@thoughtworks.com";
        this.name = name;
    }

    public User(Integer yearOfBirth) {
        this.name = "xiaoming";
        this.email = "zijie.jiang@thoughtworks.com";
        this.yearOfBirth = yearOfBirth;
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public String getEmail() {
        return email;
    }
}
